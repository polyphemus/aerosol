!-----------------------------------------------------------------------
!!     Copyright (C) 2012-2018, ENPC - EDF R&D - INERIS
!!     Author(s): Shupeng Zhu
!!
!!     This file is part of the Size Composition Resolved Aerosol Model (SCRAM), a
!!     component of the SSH-aerosol model.
!!
!!     SSH-aerosol is a free software; you can redistribute it and/or modify
!!     it under the terms of the GNU General Public License as published
!!     by the Free Software Foundation; either version 2 of the License,
!!     or (at your option) any later version.
!!
!!     SSH-aerosol is distributed in the hope that it will be useful, but
!!     WITHOUT ANY WARRANTY; without even the implied warranty of
!!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!!     General Public License for more details.
!!
!!-----------------------------------------------------------------------
!!
!!     -- DESCRIPTION
!!    This module read configuration file and initialize all global variables
!!-----------------------------------------------------------------------
module aInitialization
    implicit none
    INCLUDE 'CONST.INC'
    INCLUDE 'CONST_A.INC'
    include 'pointer.inc'
    include 'smw.inc'    ! molecular_weight_solid
    include 'solmd.inc'  ! mass_density_solid
    include 'imw.inc'    ! molecular_weight_inside
    include 'CONST_B.inc'

    !!part 1: parameters of system dimension
    Integer :: N_gas        !Complete gas species number
    integer :: N_size       !Total number of size and composition sections
    integer :: N_groups     !Number of groups
    integer :: N_fracmax    !Maximum number of composition sections per size section
    integer :: N_aerosol    !Number of aerosol species
    integer :: N_sizebin    !Number of  size sections
    integer :: N_frac       !Number of fraction of each species
    integer :: N_reaction   !Number of gas-phase reactions
    integer :: N_photolysis !Number of photolyses
    integer :: Nt           !Number of iteration
    integer :: Nmc          
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: N_organics   !Number of organics aerosol species
    integer :: N_inorganic  !Number of inorganic aerosol species
    integer :: N_inert      !number of inert aerosol species
    integer :: N_liquid     !Number of liquid internal species
    integer :: N_solid      !Number of solid internal species
    integer :: N_inside_aer !Number of internal species
    integer :: N_hydrophilic!Number of hydrophilic organics aerosol species
    !parameter (N_aerosol = N_organics + N_inorganic + N_inert + 1)
    parameter (N_organics=27,N_inorganic=5,N_inert=2,N_liquid=12)
    parameter (N_solid=9,N_inside_aer=21)
    parameter (N_hydrophilic=9)
    integer :: nesp_org_h2so4_nucl

    !!part 2: parameters of system options
  
    integer :: with_fixed_density!IDENS
    integer :: tag_init		! 0 internally mixed; 1 mixing_state resolved
    integer :: with_init_num	! 0 estimated from mass and diameter; 1 number conc. for each bin is read
    integer :: wet_diam_estimation	! 0 = isorropia ?
    integer :: tag_dbd    ! Method for defining particle size bounds (0 auto generated, 1 read)
    integer :: tag_emis	     ! 0 Without emissions 1 with internally-mixed emissions 2 with externally-mixed emissions
    integer :: with_emis_num ! 0 estimated from mass and diameter; 1 number conc. for each bin is read

    integer :: tag_external  ! 0 for internally mixed, 1 for mixing-state resolved
    integer :: kind_composition  ! 1 for auto discretization and 0 for manual discretization

    integer :: tag_chem
    integer :: with_photolysis
    integer :: with_heterogeneous  !Tag of heterogeneous reaction 

    integer :: with_adaptive       !Tag of adaptive time step for chemistry 1 if adaptive time step.
    double precision :: adaptive_time_step_tolerance !Relative tolerance for deciding if the time step is kept
    double precision :: min_adaptive_time_step       !Minimum time step
    !double precision :: DTAEROMIN !Minimum time step for aerosol dynamics
    integer :: dynamic_solver = 1 !KDSLV Tag type of solver
    integer :: sulfate_computation = 0 !ISULFCOND tag of sulfate condensation method
    integer :: redistribution_method !tag of redistribution method
    integer :: with_coag   !Tag gCoagulation
    integer :: i_compute_repart ! 0 if repartition coeff are read
    integer :: with_cond   !Tag fCondensation
    integer :: with_nucl   !Tag nucleation
    Integer :: nucl_model  !ITERN !1= Ternary, 0= binary
    integer :: ICUT        !ICUT
    double precision :: Cut_dim  !cuting diameter between equi/dynamic inorganic
    integer :: ISOAPDYN    ! organic equilibrium  = 0 or dynamic = 1
    integer :: with_oligomerization!IOLIGO
    integer :: output_type
    integer :: splitting
    integer :: computed_coag
    integer :: with_number

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    Integer :: aqueous_module!ICLD
    Integer :: with_incloud_scav!IINCLD
    integer :: with_kelvin_effect!IKELV
    integer :: section_pass
    double precision :: tequilibrium ! time under which equilibrium is assumed

    ! ! part 3: System pointers
    Integer :: E1,E2,G1,G2 !Mark the begin and end of dynamic aerosol (except EH2O)
    ! Number of different species group
    Integer, dimension(:), allocatable :: isorropia_species
    Integer, dimension(:), allocatable :: aec_species
    Integer, dimension(:), allocatable :: pankow_species
    Integer, dimension(:), allocatable :: poa_species
    Integer, dimension(:), allocatable :: org_h2so4_nucl_species
    Integer :: nesp, nesp_isorropia, nesp_aec, nesp_pankow, nesp_pom, nesp_eq_org
    ! parameter (nesp_isorropia=5,nesp_aec=19,nesp_pankow=1,nesp_pom=6, nesp_eq_org=26)
!    parameter (nesp_eq_org=26) !! YK
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Integer :: ENa,ESO4,ENH4,ENO3,ECl,EMD,EBC,EH2O!inorganic pointers
    Integer :: ictmNH3,ictmHNO3,ictmHCl,ictmSO2,ictmH2O2,ictmHCHO,ictmHNO2
    Integer :: ictmO3,ictmOH,ictmHO2,ictmNO3,ictmNO,ictmNO2,ictmPAN,ictmH2SO4
    ! pointers of cloud species.



    !!part 4: System state parameters    
    ! time setting
    double precision :: final_time,dt,time_emis,delta_t, initial_time  

    double precision :: Temperature,Relative_Humidity,Pressure,Humidity, pH, pressure_sat
    double precision :: longitude, latitude
    double precision :: attenuation
    double precision :: fixed_density
    double precision :: lwc_cloud_threshold

    integer :: tag_coag,tag_cond,tag_nucl

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    double precision :: viscosity!Dynamic viscosity ([kg/m/s]).
    double precision :: air_free_mean_path
    double precision :: total_water!total mass of water
    double precision :: total_IH!total mass of H+
    double precision :: total_PH!overall PH value
    double precision :: n_emis
    double precision :: m_emis
    double precision :: aero_total_mass 
    Double precision,dimension(:), allocatable :: total_mass!, total_mass_old!total mass of each species
    Double precision,dimension(:), allocatable :: discretization_mass
    Double precision :: p_fact,k_fact
    Double precision :: DQLIMIT

    !!part5: dimension data array    
    integer, dimension(:), allocatable :: Index_groups	!index of which group the species belongs to
    integer, dimension(:), allocatable :: List_species	!read species defined in cfg files
    Integer, dimension(:), allocatable :: aerosol_species_interact
    integer, dimension(:), allocatable :: N_fracbin	!vector of number of composition sections for each section

    Double precision,dimension(:), allocatable :: photolysis
    integer, dimension(:), allocatable :: photolysis_reaction_index

    Double precision,dimension(:), allocatable :: density_aer_bin 	!density of each grid bins
    Double precision,dimension(:), allocatable :: density_aer_size 	!density of each size section
    Double precision , dimension(:), allocatable :: rho_wet_cell

    Double precision,dimension(:), allocatable :: diam_bound	! DBF diameter bounds of each size section
    double precision,dimension(:), allocatable :: diam_input
    double precision,dimension(:), allocatable :: frac_bound
    double precision,dimension(:), allocatable :: frac_input


    Double precision,dimension(:), allocatable :: size_diam_av	!DSF average diameter of each size section
    Double precision,dimension(:), allocatable :: size_mass_av	!MSF average mass of each size section
    !Double precision,dimension(:), allocatable :: size_log_av	!XSF
    Double precision,dimension(:), allocatable :: cell_diam_av	!DSF average diameter of each grid cell
    Double precision,dimension(:), allocatable :: cell_mass_av	!MSF average mass of each grid cell
    Double precision,dimension(:), allocatable :: cell_log_av	!XSF

    DOUBLE PRECISION, dimension(:), allocatable :: concentration_gas_all
    Double precision,dimension(:), allocatable :: concentration_gas	! gas concentration of each species
    integer, dimension(:,:), allocatable :: concentration_index !matrix from grid index to size and composition index
    integer, dimension(:,:), allocatable :: concentration_index_iv !matrix from size and composition to grid index
    Double precision,dimension(:), allocatable :: concentration_number	!number concentration of each grid cell
    double precision , dimension(:,:), allocatable :: concentration_mass

    double precision, dimension(:), allocatable    :: gas_emis !storing Gas consentration (emission) micm^3cm^-3 
    double precision,dimension(:,:), allocatable   :: init_bin_mass
    double precision,dimension(:), allocatable     :: init_mass
    double precision,dimension(:,:), allocatable   :: emis_bin_mass
    double precision,dimension(:), allocatable     :: init_bin_number 
    double precision,dimension(:), allocatable     :: emis_bin_number

    double precision , dimension(:,:), allocatable :: emission_rate
    double precision , dimension(:), allocatable   :: emission_num_rate

    Double precision,dimension(:), allocatable :: wet_diameter	!Aerosol wet diameter (�m). of each grid cell
    Double precision,dimension(:), allocatable :: wet_mass	!Aerosol wet mass (�g). of each grid cell
    Double precision,dimension(:), allocatable :: wet_volume	!Aerosol wet volume (�m^3). of each grid cell
    double precision , dimension(:,:,:), allocatable :: discretization_composition! multi-array storing discretization of composition

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    Double precision,dimension(:), allocatable :: mass_bound! MBF
    Double precision,dimension(:), allocatable :: log_bound!XBF
    Double precision,dimension(:), allocatable :: total_bin_mass!total mass of each size section
    Double precision,dimension(:), allocatable :: size_sect!HSF log size of each section


    Double precision,dimension(:), allocatable :: mass_total_grid!total mass of each grid cell
    Double precision,dimension(:), allocatable :: total_aero_mass!total aerosol mass of each species
    Double precision,dimension(:), allocatable :: bin_mass!mass concentration of each size section
    Double precision,dimension(:), allocatable :: bin_number!number concentration of each size section
    Double precision,dimension(:), allocatable :: concentration_number_tmp!first order approximation of number

    Double precision , dimension(:), allocatable :: cell_mass
    double precision, dimension(:), allocatable :: number_init  !for each sizebin
    !double precision, dimension(:), allocatable :: mass_init    !for each sizebin


    double precision, dimension(:), allocatable :: per_mass_init!initial percentage of each species within aerosol



    double precision,dimension(:), allocatable:: gas_mass_init


    double precision , dimension(:,:), allocatable :: kernel_coagulation
    double precision , dimension(:,:), allocatable :: ce_kernal_coef!c/e kernal
    double precision , dimension(:,:), allocatable :: Kelvin_effect_ext!kelvin effect
    double precision , dimension(:,:), allocatable :: frac_grid !excat fraction of each species in each grid

    double precision , dimension(:,:), allocatable :: concentration_mass_tmp!first order apporximation
    double precision , dimension(:,:), allocatable :: concentration_inti!internal inorganic aerosol concentration ([�g.m-3]).
    double precision , dimension(:,:), allocatable :: dqdt



    !! part 7: basic physical and chemical parameters
    !double precision :: SMD(SNaNO3:SLC) = ()!molar weight of internal solids species
    !double precision :: IMW(N_liquid)!molar weight of inorganic species in aqueous_phase
    !double precision :: SMW(SNaNO3:SLC)!molar weight of solids
    double precision ,dimension(:), allocatable :: accomodation_coefficient
    double precision ,dimension(:), allocatable :: surface_tension
    double precision ,dimension(:), allocatable :: molecular_weight_aer! (�g/mol)
    double precision ,dimension(:), allocatable :: molecular_diameter
    double precision ,dimension(:), allocatable :: collision_factor_aer
    double precision ,dimension(:), allocatable :: mass_density!(�g/m3) liquid mass density
    double precision ,dimension(:), allocatable :: quadratic_speed! (m.s-1)
    double precision ,dimension(:), allocatable :: diffusion_coef! (m2.s-1)
    double precision ,dimension(:), allocatable :: soa_sat_conc! (�g.m-3)
    double precision ,dimension(:), allocatable :: soa_part_coef!(m3/microg)
    double precision ,dimension(:), allocatable :: molecular_weight! (�g/mol) gas=phase

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer, dimension(:), allocatable :: Ncoefficient, index_first, index_second
    double precision, dimension(:), allocatable :: coefficient
    integer :: coef_size
    double precision :: surface_tension_inorg, surface_tension_aq, surface_tension_org

    double precision :: dorg
    integer :: coupled_phases
    integer :: nlayer
    integer :: activity_model
    double precision, dimension(:), allocatable :: lwc_nsize, &
         ionic_nsize, proton_nsize, chp_nsize
    double precision, dimension(:,:), allocatable :: liquid_nsize
    
    !! part 8: divers parameters (species, I/O)
    character (len=40) :: Coefficient_file ! repartition coefficient file
    character (len=40) :: init_aero_conc_mass_file ! File for aeroslos initial mass concentrations
    character (len=40) :: init_aero_conc_num_file ! File for aerosols initial number concentrations
    character (len=40) :: init_gas_conc_file ! File for gas-phase initial conc.
    character (len=40) :: species_list_file ! File for species list.
    character (len=40) :: aerosol_species_list_file ! File for species list.
    character (len=40) :: namelist_species ! Namelist file for species list.
    character (len=40) :: emis_gas_file
    character (len=40) :: emis_aero_mass_file
    character (len=40) :: emis_aero_num_file
    character (len=40) :: mineral_dust(10), black_carbon(10)
    character (len=40) :: isorropia_species_name(10)
    character (len=40) :: aec_species_name(30)
    character (len=40) :: pankow_species_name(6)
    character (len=40) :: poa_species_name(10)
    character (len=40) :: PSO4
    character (len=10) :: precursor
    character (len=10), dimension(:), allocatable :: species_name
    character (len=20), dimension(:), allocatable :: aerosol_species_name
    character (len=10), dimension(:), allocatable :: emis_gas_species_name
    character (len=10), dimension(:), allocatable :: emis_aer_species_name
    character (len=100) :: output_directory, output_dir2


    !!part 6: used in ssh-aerosol.f90 chem()
    integer :: ns_source
    integer, dimension(:), allocatable :: source_index
    double precision, dimension(:), allocatable :: source  
    double precision, dimension(:), allocatable :: conversionfactor
    double precision, dimension(:,:), allocatable :: conversionfactorjacobian
    ! Array of chemical volumic emissions at final time ([\mu.g/m^3/s]).
    integer :: heterogeneous_reaction_index(4)
    integer :: ind_jbiper, ind_kbiper   
    ! To take into account BiPER degradation inside the particle
    

#ifdef POLYPHEMUS_PARALLEL_WITH_OPENMP
!$omp threadprivate(N_gas,N_size,N_groups,N_fracmax,N_aerosol,N_sizebin)
!$omp threadprivate(tag_thrm,dynamic_solver,sulfate_computation,redistribution_method)
!$omp threadprivate(with_coag,with_cond,with_nucl,aqueous_module,with_incloud_scav)
!$omp threadprivate(with_kelvin_effect,with_fixed_density,ICUT,nucl_model,wet_diam_estimation)
!$omp threadprivate(section_pass,with_oligomerization,thermodynamic_model,with_number)
!$omp threadprivate(E1,E2,G1,G2,ENa,ESO4,ENH4,ENO3,ECl,EMD,EBC,EH2O)
!$omp threadprivate(nesp, nesp_isorropia, nesp_aec, nesp_pankow, nesp_pom)
!$omp threadprivate(tagrho,tag_coag,tag_cond,tag_nucl,kind_composition,timestep_splitting)
!$omp threadprivate(sub_timestep_splitting,final_time,dtmin,initial_time_splitting,current_sub_time)
!$omp threadprivate(final_sub_time,Temperature,Relative_Humidity,Pressure,Humidity)
!$omp threadprivate(fixed_density,Cut_dim,viscosity,air_free_mean_path)
!$omp threadprivate(total_water,total_IH,total_PH,n_grow_nucl,n_grow_coag,n_emis)
!$omp threadprivate(m_grow_cond,m_emis,o_total_mass,total_mass_t)
!$omp threadprivate(DQLIMIT,record_time,p_fact,k_fact)
!$omp threadprivate(mass_density_solid,molecular_weight_inside,molecular_weight_solid)
!$omp threadprivate(isorropia_species,aec_species,pankow_species,poa_species,org_h2so4_nucl_species)
!$omp threadprivate(Index_groups,List_species,aerosol_species_interact,density_aer_bin)
!$omp threadprivate(density_aer_size,diam_bound,mass_bound,log_bound,total_bin_mass)
!$omp threadprivate(size_sect,size_diam_av,size_mass_av,size_log_av,cell_diam_av)
!$omp threadprivate(cell_mass_av,cell_log_av,total_mass,mass_total_grid,cell_mass)
!$omp threadprivate(total_aero_mass,bin_mass,bin_number,concentration_number_tmp,rho_wet_cell)
!$omp threadprivate(concentration_number,concentration_gas,wet_diameter,wet_mass,wet_volume)
!$omp threadprivate(concentration_index,concentration_index_iv,kernel_coagulation,ce_kernal_coef)
!$omp threadprivate(Kelvin_effect_ext,frac_grid,concentration_mass,concentration_mass_tmp)
!$omp threadprivate(concentration_inti,dqdt,discretization_composition)
!$omp threadprivate(accomodation_coefficient)
!$omp threadprivate(surface_tension,molecular_weight_aer)
!$omp threadprivate(molecular_diameter,collision_factor_aer,mass_density)
!$omp threadprivate(quadratic_speed,diffusion_coef,soa_sat_conc,soa_part_coef)
#endif

  ! Parameter and variable definitions
  type :: ptr_to_real_array
     integer n
     double precision, dimension(:), pointer :: arr
  end type ptr_to_real_array

  type :: ptr_to_integer_array
     integer n
     integer, dimension(:), pointer :: arr
  end type ptr_to_integer_array

  ! Repartition coefficients.
  type(ptr_to_real_array), dimension(:), allocatable :: repartition_coefficient
  type(ptr_to_real_array), dimension(:), allocatable :: repartition_coefficient_nc
  ! Index of repartition coefficients.
  type(ptr_to_integer_array), dimension(:), allocatable :: index1_repartition_coefficient
  type(ptr_to_integer_array), dimension(:), allocatable :: index1_repartition_coefficient_nc
  type(ptr_to_integer_array), dimension(:), allocatable :: index2_repartition_coefficient
  type(ptr_to_integer_array), dimension(:), allocatable :: index2_repartition_coefficient_nc
#ifdef POLYPHEMUS_PARALLEL_WITH_OPENMP
!$omp threadprivate(repartition_coefficient,repartition_coefficient_nc)
!$omp threadprivate(index1_repartition_coefficient,index1_repartition_coefficient_nc)
!$omp threadprivate(index2_repartition_coefficient,index2_repartition_coefficient_nc)
#endif

    
 contains

    subroutine Init_global_parameters(nesp_loc,nbin_aer,nsize_section_aer,nesp_aer,&
				     ncomposition_aer,ngroup_aer,&
				     aerosol_species_interact_loc,&
				     nesp_isorropia_loc,isorropia_species_loc,&
				     nesp_aec_loc,aec_species_loc,&
				     nesp_pankow_loc,pankow_species_loc,&
				     nesp_pom_loc,poa_species_loc,&
                                     nesp_org_h2so4_nucl_loc,org_h2so4_nucl_species_loc,&
				     md_species_loc, bc_species_loc,&
				     nesp_cloud_interact,cloud_species_interact,&
				     saturation_pressure_loc,partition_coefficient_loc,&
				     vaporization_enthalpy_loc,accomodation_coefficient_loc,&
				     surface_tension_loc,saturation_pressure_mass_loc,&
				     saturation_pressure_torr_loc,&
				     deliquescence_relative_humidity_loc,molecular_weight_aer_loc,&
				     molecular_diameter_aer_loc,collision_factor_aer_loc,&
				     mass_density_aer_loc,noptions_aer,options_aer,&
				     aerosol_species_group_relation)
!------------------------------------------------------------------------
!
!     -- DESCRIPTION
!     This subroutine initialize all global variables (Only used in 3D model)
!
!------------------------------------------------------------------------
!
!     -- INPUT VARIABLES
!
!------------------------------------------------------------------------
    integer b,jesp,j,f,k
    integer :: nesp_aer,nbin_aer,nsize_section_aer,nesp_loc
    integer :: aerosol_species_interact_loc(nesp_aer)
    integer :: nesp_isorropia_loc,nesp_org_h2so4_nucl_loc
    integer :: ncomposition_aer,ngroup_aer
    integer :: isorropia_species_loc(nesp_isorropia_loc)
    integer :: nesp_aec_loc,aec_species_loc(nesp_aec_loc)
    integer :: nesp_pankow_loc,pankow_species_loc(nesp_pankow_loc)
    integer :: nesp_pom_loc,poa_species_loc(nesp_pom_loc)
    integer :: md_species_loc,bc_species_loc
    integer :: nesp_cloud_interact
    integer :: cloud_species_interact(nesp_cloud_interact)
    integer :: noptions_aer,options_aer(noptions_aer)
    integer :: aerosol_species_group_relation(nesp_aer)
    integer :: org_h2so4_nucl_species_loc(nesp_org_h2so4_nucl_loc)
    double precision :: saturation_pressure_loc(nesp_aer)
    double precision :: partition_coefficient_loc(nesp_aer)
    double precision :: vaporization_enthalpy_loc(nesp_aer)
    double precision :: accomodation_coefficient_loc(nesp_aer)
    double precision :: surface_tension_loc(nesp_aer)
    double precision :: saturation_pressure_mass_loc(nesp_aer)
    double precision :: saturation_pressure_torr_loc(nesp_aer)
    double precision :: deliquescence_relative_humidity_loc(nesp_aer)
    double precision :: molecular_weight_aer_loc(nesp_aer)
    double precision :: molecular_diameter_aer_loc(nesp_aer)
    double precision :: collision_factor_aer_loc(nesp_aer)
    double precision :: mass_density_aer_loc(nesp_aer)

!! basic parameters (system dimensions)
    N_gas=nesp_loc
    nesp=nesp_loc
    N_aerosol=nesp_aer
    N_size=nbin_aer
    N_fracmax=ncomposition_aer
    N_groups=ngroup_aer
    N_sizebin=nsize_section_aer

    allocate(total_mass(N_aerosol))
    allocate(concentration_gas(N_aerosol))
    allocate(concentration_number(N_size))
    allocate(concentration_number_tmp(N_size))
    allocate(concentration_mass(N_size,N_aerosol))
    allocate(concentration_mass_tmp(N_size,N_aerosol))
    allocate(concentration_inti(N_size,N_inside_aer))
    allocate(cell_mass_av(N_size))
    allocate(cell_mass(N_size))
    allocate(cell_diam_av(N_size))
    allocate(cell_log_av(N_size))
    allocate(total_aero_mass(N_aerosol))
    allocate(mass_total_grid(N_size))
    allocate(Index_groups(N_aerosol-1))
    allocate(wet_mass(N_size))
    allocate(wet_diameter(N_size))
    allocate(wet_volume(N_size))
    allocate(size_diam_av(N_sizebin))
    allocate(size_mass_av(N_sizebin))
    allocate(size_sect(N_sizebin))
    allocate(diam_bound(N_sizebin+ 1))
    allocate(mass_bound(N_sizebin+ 1))
    allocate(log_bound(N_sizebin+ 1))
    allocate(density_aer_bin(N_size))
    allocate(density_aer_size(N_sizebin))
    allocate(bin_mass(N_sizebin))
    allocate(bin_number(N_sizebin))
    allocate(rho_wet_cell(N_size))
    allocate(kernel_coagulation(N_size,N_size))
    allocate(total_bin_mass(N_sizebin))
    allocate(ce_kernal_coef(N_size,N_aerosol))
    allocate(Kelvin_effect_ext(N_size,N_aerosol))
    allocate(frac_grid(N_size,N_groups))
    allocate(dqdt(N_size,N_aerosol))
    
    total_mass=0.d0
    concentration_gas=0.d0
    concentration_number=0.d0
    concentration_number_tmp=0.d0
    concentration_mass=0.d0
    concentration_mass_tmp=0.d0
    concentration_inti=0.d0
    total_aero_mass=0.d0
    cell_mass_av=0.d0
    cell_diam_av=0.d0
    cell_log_av=0.d0
    mass_total_grid=0.d0
    wet_mass=0.d0
    wet_diameter=0.d0
    wet_volume=0.d0
    size_diam_av=0.d0
    size_mass_av=0.d0
    size_sect=0.d0
    diam_bound=0.d0
    mass_bound=0.d0
    log_bound=0.d0
    density_aer_bin=0.d0
    density_aer_size=0.d0
 
    !!read relations between species and chemical groups
    do jesp =1,(N_aerosol-1)
      Index_groups(jesp)=aerosol_species_group_relation(jesp)+1
      !!c++ to frotran
    enddo

    !!set index relation between bins and size&composition
    allocate(concentration_index_iv(N_sizebin,N_fracmax))
    allocate(concentration_index(N_size,2))
    do b = 1,N_sizebin
      do f = 1,N_fracmax
	j=N_fracmax*(b-1)+f
	concentration_index_iv(b,f)=j
	concentration_index(j,1)=b
	concentration_index(j,2)=f
      enddo
     enddo

!!     Set parameters
    with_coag = options_aer(1)
    with_cond = options_aer(2)
    with_nucl = options_aer(3)
    aqueous_module = options_aer(4)
    with_incloud_scav = options_aer(5)
    with_kelvin_effect = options_aer(6)
    if (options_aer(7) .eq. 1) then
       with_fixed_density = 0
    else
       with_fixed_density = 1
    endif !! Fixed a bug :: YK
    ! with_fixed_density = options_aer(7)
    ICUT = options_aer(8)
    sulfate_computation = options_aer(9)
    dynamic_solver = options_aer(10)
    redistribution_method = options_aer(11)
    nucl_model = options_aer(12)
    wet_diam_estimation = options_aer(13)
    with_oligomerization = options_aer(14)
!    thermodynamic_model = options_aer(15)
    ISOAPDYN = options_aer(15)
    with_number = options_aer(16)
    computed_coag = options_aer(17) !! YK
    with_init_num = options_aer(18)
    splitting = options_aer(19) !! YK
     
    !     Species to be computed
    IF(ICUT.GT.0.D0) THEN
      section_pass=concentration_index(ICUT,1)
    ELSE
      section_pass=1
    ENDIF
    
    E1=1
    E2=nesp_aer-1 ! to avoid water

    nesp_isorropia=nesp_isorropia_loc
    nesp_aec=nesp_aec_loc
    nesp_pankow=nesp_pankow_loc
    nesp_pom=nesp_pom_loc
    nesp_org_h2so4_nucl=nesp_org_h2so4_nucl_loc
    nesp_eq_org = nesp_aec_loc + nesp_pankow_loc + nesp_pom_loc !! YK
    
    !!initialize pointers
    allocate(isorropia_species(nesp_isorropia_loc))
    allocate(aec_species(nesp_aec_loc))
    allocate(pankow_species(nesp_pankow_loc))
    allocate(poa_species(nesp_pom))
    allocate(aerosol_species_interact(nesp_aer))
    allocate(List_species(N_aerosol))
    allocate(org_h2so4_nucl_species(nesp_org_h2so4_nucl_loc))

    ENa=isorropia_species_loc(1)
    ESO4=isorropia_species_loc(2)
    ENH4=isorropia_species_loc(3)
    ENO3=isorropia_species_loc(4)
    ECl=isorropia_species_loc(5)
    EMD=md_species_loc
    EBC=bc_species_loc
    EH2O=nesp_aer ! water always at the end
    
    !initialized the aerosol species pointer list without water
    List_species(1)=EMD
    List_species(2)=EBC

    do jesp=1,nesp_isorropia
      isorropia_species(jesp) = isorropia_species_loc(jesp)
      List_species(2+jesp)=isorropia_species_loc(jesp)
    enddo

    do jesp=1,nesp_aec
      aec_species(jesp) = aec_species_loc(jesp)
      List_species(2+nesp_isorropia+jesp)=aec_species_loc(jesp)
    enddo

    do jesp=1,nesp_pankow
      pankow_species(jesp) = pankow_species_loc(jesp)
      List_species(2+nesp_isorropia+nesp_aec+jesp)=pankow_species_loc(jesp)
    enddo

    do jesp=1,nesp_pom
      poa_species(jesp) = poa_species_loc(jesp)
      List_species(2+nesp_isorropia+nesp_aec+nesp_pankow+jesp)=poa_species_loc(jesp)	
    enddo

    List_species(N_aerosol)=EH2O
   
    do jesp=1,nesp_org_h2so4_nucl
      org_h2so4_nucl_species(jesp) = org_h2so4_nucl_species_loc(jesp)
    enddo

    G1=ESO4
    G2=ECl

    do jesp=1,nesp_aer
	aerosol_species_interact(jesp)=aerosol_species_interact_loc(jesp)
    enddo

    !! Pointers for cloud species.

    ictmNH3=cloud_species_interact(1)
    ictmHNO3=cloud_species_interact(2)
    ictmHCl=cloud_species_interact(3)
    ictmSO2=cloud_species_interact(4)
    ictmH2O2=cloud_species_interact(5)
    ictmHCHO=cloud_species_interact(6)
    ictmHNO2=cloud_species_interact(7)
    ictmO3=cloud_species_interact(8)
    ictmOH=cloud_species_interact(9)
    ictmHO2=cloud_species_interact(10)
    ictmNO3=cloud_species_interact(11)
    ictmNO=cloud_species_interact(12)
    ictmNO2=cloud_species_interact(13)
    ictmPAN=cloud_species_interact(14)
    ictmH2SO4=cloud_species_interact(15)

    !!initialize basic physical and chemical parameters
    allocate(accomodation_coefficient(nesp_aer))
    allocate(surface_tension(nesp_aer))
    allocate(molecular_weight_aer(nesp_aer))
    allocate(molecular_diameter(nesp_aer))
    allocate(collision_factor_aer(nesp_aer))
    allocate(mass_density(nesp_aer))
    allocate(quadratic_speed(nesp_aer))
    allocate(diffusion_coef(nesp_aer))
    allocate(soa_sat_conc(nesp_aer))
    allocate(soa_part_coef(nesp_aer))

    do jesp = 1,nesp_aer
      surface_tension(jesp)=surface_tension_loc(jesp)
      accomodation_coefficient(jesp)=accomodation_coefficient_loc(jesp)
      molecular_weight_aer(jesp)=molecular_weight_aer_loc(jesp)
      molecular_diameter(jesp)=molecular_diameter_aer_loc(jesp)
      collision_factor_aer(jesp)=collision_factor_aer_loc(jesp)
      mass_density(jesp)=mass_density_aer_loc(jesp)!�g/�m3 ?
      soa_part_coef(jesp)=0.d0
      soa_sat_conc(jesp)=0.d0
      diffusion_coef(jesp)=0.d0
      quadratic_speed(jesp)=0.d0
    enddo


    allocate(lwc_nsize(n_size)) ! SOAP !
    lwc_nsize = 0.d0
    allocate(ionic_nsize(n_size)) ! SOAP !
    ionic_nsize = 0.d0
    allocate(proton_nsize(n_size))  ! SOAP !
    proton_nsize = 0.d0
    allocate(chp_nsize(n_size))  ! SOAP !
    chp_nsize = 0.d0
    allocate(liquid_nsize(12,n_size))   ! SOAP !
    liquid_nsize = 0.d0
    
    
  END subroutine Init_global_parameters

  subroutine free_allocated_memory()
    
    integer ierr


    deallocate(total_mass)
    deallocate(cell_mass)
    deallocate(concentration_gas)
    deallocate(concentration_number)
    deallocate(concentration_number_tmp)
    deallocate(concentration_mass)
    deallocate(concentration_mass_tmp)
    deallocate(concentration_inti)
    deallocate(cell_mass_av)
    deallocate(cell_diam_av)
    deallocate(cell_log_av)
    deallocate(total_aero_mass)
    deallocate(mass_total_grid)
    deallocate(Index_groups)
    deallocate(wet_mass)
    deallocate(wet_diameter)
    deallocate(wet_volume)
    deallocate(size_diam_av)
    deallocate(size_mass_av)
    deallocate(size_sect)
    deallocate(diam_bound)
    deallocate(mass_bound)
    if (allocated(log_bound)) then
       deallocate(log_bound, stat=ierr)
    endif
    if (allocated(density_aer_bin)) then
       deallocate(density_aer_bin, stat=ierr)
    endif
    if (allocated(density_aer_size)) then
       deallocate(density_aer_size, stat=ierr)
       if (ierr .ne. 0) then
          stop "Deallocation error"
       endif
    endif
    deallocate(bin_mass)
    deallocate(bin_number)
    deallocate(rho_wet_cell)
    deallocate(kernel_coagulation)
    deallocate(total_bin_mass)
    deallocate(ce_kernal_coef)
    deallocate(Kelvin_effect_ext)
    deallocate(frac_grid)
    deallocate(dqdt)
    deallocate(concentration_index_iv)
    deallocate(concentration_index)
    deallocate(isorropia_species)
    deallocate(aec_species)
    deallocate(pankow_species)
    deallocate(poa_species)
    deallocate(org_h2so4_nucl_species)
    deallocate(aerosol_species_interact)
    deallocate(List_species)
    deallocate(accomodation_coefficient)
    deallocate(surface_tension)
    deallocate(molecular_weight_aer)
    deallocate(molecular_diameter)
    deallocate(collision_factor_aer)
    deallocate(mass_density)
    deallocate(quadratic_speed)
    deallocate(diffusion_coef)
    if (allocated(discretization_mass)) deallocate(discretization_mass) !! YK
    deallocate(soa_sat_conc)
    deallocate(soa_part_coef)
    deallocate(discretization_composition)
    if (allocated(lwc_nsize)) deallocate(lwc_nsize) !! YK
    if (allocated(ionic_nsize)) deallocate(ionic_nsize) !! YK
    if (allocated(proton_nsize)) deallocate(proton_nsize) !! YK
    if (allocated(chp_nsize)) deallocate(chp_nsize) !! YK
    if (allocated(liquid_nsize)) deallocate(liquid_nsize) !! YK
  END subroutine free_allocated_memory

end module aInitialization
